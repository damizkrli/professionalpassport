<?php

    namespace App\DataFixtures;

    use App\Entity\Projects;
    use Doctrine\Bundle\FixturesBundle\Fixture;
    use Doctrine\Persistence\ObjectManager;

    class AppFixtures extends Fixture
    {
        public function load(ObjectManager $manager): void
        {
            for ($i = 0; $i < 10; $i++) {
                $project = new Projects();
                $project->setName('Projet' . $i);
                $project->setDescription('blablabla');
                $project->setTeam(true);
                $project->setCreatedAt(new \DateTimeImmutable('12/12/2021'));
                $project->setUpdatedAt(new \DateTimeImmutable('12/12/2021'));
                $manager->persist($project);
            }

            $manager->flush();
        }
    }
