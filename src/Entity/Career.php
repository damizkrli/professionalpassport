<?php

namespace App\Entity;

use App\Repository\CareerRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: CareerRepository::class)]
class Career
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(type: 'string', length: 100)]
    private ?string $title;

    #[ORM\Column(type: 'string', length: 100)]
    private ?string $company;

    #[ORM\Column(type: 'date')]
    private ?\DateTimeInterface $start;

    #[ORM\Column(type: 'date')]
    private ?\DateTimeInterface $end;

    #[ORM\Column(type: 'boolean')]
    private ?bool $currentJob;

    #[ORM\Column(type: 'text')]
    private ?string $mission;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getCompany(): ?string
    {
        return $this->company;
    }

    public function setCompany(string $company): self
    {
        $this->company = $company;

        return $this;
    }

    public function getStart(): ?\DateTimeInterface
    {
        return $this->start;
    }

    public function setStart(\DateTimeInterface $start): self
    {
        $this->start = $start;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getCurrentJob(): ?bool
    {
        return $this->currentJob;
    }

    public function setCurrentJob(bool $currentJob): self
    {
        $this->currentJob = $currentJob;

        return $this;
    }

    public function getMission(): ?string
    {
        return $this->mission;
    }

    public function setMission(string $mission): self
    {
        $this->mission = $mission;

        return $this;
    }
}
