<?php

    namespace App\Entity;

    use App\Repository\ProjectsRepository;
	use Doctrine\Common\Collections\ArrayCollection;
	use Doctrine\ORM\Mapping as ORM;

    #[ORM\Entity(repositoryClass: ProjectsRepository::class)]
    class Projects
    {
		#[ORM\Id]
        #[ORM\GeneratedValue]
        #[ORM\Column(type: 'integer')]
        private $id;

        #[ORM\Column(type: 'string', length: 75)]
        private ?string $name;

        #[ORM\Column(type: 'string', length: 255, nullable: true)]
        private ?string $image;

        #[ORM\Column(type: 'text')]
        private ?string $description;

        #[ORM\Column(type: 'boolean')]
        private ?bool $team = false;

        #[ORM\Column(type: 'datetime_immutable', nullable: true)]
        private ?\DateTimeImmutable $createdAt;

        #[ORM\Column(type: 'datetime_immutable', nullable: true)]
        private ?\DateTimeImmutable $updatedAt;

        #[ORM\Column(type: 'integer', nullable: true)]
        private ?int $progress = 0;

        #[ORM\Column(type: 'simple_array')]
        private array $languages = [];

        public function getId(): ?int
        {
            return $this->id;
        }

        public function getName(): ?string
        {
            return $this->name;
        }

        public function setName(string $name): self
        {
            $this->name = $name;

            return $this;
        }

        public function getImage(): ?string
        {
            return $this->image;
        }

        public function setImage(string $image): self
        {
            $this->image = $image;

            return $this;
        }

        public function getDescription(): ?string
        {
            return $this->description;
        }

        public function setDescription(string $description): self
        {
            $this->description = $description;

            return $this;
        }

        public function getTeam(): ?bool
        {
            return $this->team;
        }

        public function setTeam(bool $team): self
        {
            $this->team = $team;

            return $this;
        }

        public function getCreatedAt(): ?\DateTimeImmutable
        {
            return $this->createdAt;
        }

        public function setCreatedAt(\DateTimeImmutable $createdAt): self
        {
            $this->createdAt = $createdAt;

            return $this;
        }

        public function getUpdatedAt(): ?\DateTimeImmutable
        {
            return $this->updatedAt;
        }

        public function setUpdatedAt(\DateTimeImmutable $updatedAt): self
        {
            $this->updatedAt = $updatedAt;

            return $this;
        }

        public function getProgress(): ?int
        {
            return $this->progress;
        }

        public function setProgress(?int $progress): self
        {
            $this->progress = $progress;

            return $this;
        }

        public function getLanguages(): ?array
        {
            return $this->languages;
        }

        public function setLanguages(array $languages): self
        {
            $this->languages = $languages;

            return $this;
        }


	}
