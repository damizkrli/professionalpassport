<?php

    namespace App\EventSubscriber;

    use App\Entity\Projects;
    use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
    use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
    use JetBrains\PhpStorm\ArrayShape;
    use Symfony\Component\EventDispatcher\EventSubscriberInterface;

    class AdminSubscriber implements EventSubscriberInterface
    {
        /**
         * @inheritDoc
         */
        #[ArrayShape([BeforeEntityPersistedEvent::class => "string[]"])] public static function getSubscribedEvents(): array
        {
            return [
                    BeforeEntityPersistedEvent::class => ['setCreatedAt'],
                    BeforeEntityUpdatedEvent::class => ['setUpdatedAt'],
            ];
        }

        public function setCreatedAt(BeforeEntityPersistedEvent $event): void
        {
            $entityInstance = $event->getEntityInstance();

            if (!$entityInstance instanceof Projects) {
                return;
            }

            $entityInstance->setCreatedAt(new \DateTimeImmutable());
        }

        public function setUpdatedAt(BeforeEntityUpdatedEvent $event): void
        {
            $entityInstance = $event->getEntityInstance();

            if (!$entityInstance instanceof Projects) {
                return;
            }

            $entityInstance->setUpdatedAt(new \DateTimeImmutable());
        }
    }
