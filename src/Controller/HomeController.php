<?php

    namespace App\Controller;

    use App\Repository\CareerRepository;
    use App\Repository\HardSkillsRepository;
    use App\Repository\HobbyRepository;
    use App\Repository\ProjectsRepository;
    use App\Repository\SoftSkillsRepository;
    use App\Repository\StudyRepository;
    use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
    use Symfony\Component\HttpFoundation\Response;
    use Symfony\Component\Routing\Annotation\Route;

    class HomeController extends AbstractController
    {
        public function __construct(
            ProjectsRepository $repository,
            StudyRepository $studyRepository,
            HobbyRepository $hobbyRepository,
            CareerRepository $careerRepository,
            HardSkillsRepository $hardSkillsRepository,
        ) {
        }

        #[Route('/', name: 'home')]
        public function index(
            ProjectsRepository $repository,
            StudyRepository $studyRepository,
            HobbyRepository $hobbyRepository,
            CareerRepository $careerRepository,
            HardSkillsRepository $hardSkillsRepository,
            SoftSkillsRepository $softSkillsRepository,
        ): Response {
            $project = $repository->findAll();
            $study = $studyRepository->findAll();
            $hobby = $hobbyRepository->findAll();
            $career = $careerRepository->findAll();
            $hardSkill = $hardSkillsRepository->findAll();
            $softSkill = $softSkillsRepository->findAll();

            return $this->render('home/index.html.twig', [
                'project' => $project,
                'studies' => $study,
                'hobbies' => $hobby,
                'careers' => $career,
                'hardSkills' => $hardSkill,
                'softSkills' => $softSkill,
            ]);
        }
    }
