<?php

namespace App\Controller\Admin;

use App\Entity\Career;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class CareerCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Career::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id')->onlyOnIndex(),
            TextField::new('title', 'Job title'),
            TextField::new('company', 'Company'),
            DateField::new('start', 'Start')->renderAsNativeWidget(),
            DateField::new('end', 'End')->renderAsNativeWidget(),
            BooleanField::new('currentJob', 'Still on the job')->renderAsSwitch(false),
            TextEditorField::new('mission', 'Job description')
        ];
    }

}
