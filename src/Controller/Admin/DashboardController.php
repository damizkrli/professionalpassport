<?php

namespace App\Controller\Admin;

use App\Entity\Career;
use App\Entity\HardSkills;
use App\Entity\Hobby;
use App\Entity\Projects;
use App\Entity\SoftSkills;
use App\Entity\Study;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    public function __construct(
        private AdminUrlGenerator $adminUrlGenerator,
    ) {
    }

    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        // return $this->render('some/path/my-dashboard.html.twig');

        $url = $this->adminUrlGenerator
            ->setController(CareerCrudController::class)
            ->generateUrl();

        return $this->redirect($url);
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Curriculum Vitae');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linkToRoute('Back to Home', 'fas fa-arrow-left', 'home');

        yield MenuItem::section('Jobs');
        yield MenuItem::subMenu('Jobs', 'fas fa-briefcase')->setSubItems([
            MenuItem::linkToCrud('Show jobs', 'fas fa-eye', Career::class)->setAction(Crud::PAGE_INDEX),
            MenuItem::linkToCrud('Add jobs', 'fas fa-plus', Career::class)->setAction(Crud::PAGE_NEW)
        ]);

        yield MenuItem::section('Projects');
        yield MenuItem::subMenu('Projects', 'fas fa-folder')->setSubItems([
            MenuItem::linkToCrud('Show projects', 'fas fa-eye', Projects::class)->setAction(Crud::PAGE_INDEX),
            MenuItem::linkToCrud('Add projects', 'fas fa-plus', Projects::class)->setAction(Crud::PAGE_NEW)
        ]);

        yield MenuItem::section('Studies');
        yield MenuItem::subMenu('Studies', 'fas fa-school')->setSubItems([
            MenuItem::linkToCrud('Show programs', 'fas fa-eye', Study::class)->setAction(Crud::PAGE_INDEX),
            MenuItem::linkToCrud('Add programs', 'fas fa-plus', Study::class)->setAction(Crud::PAGE_NEW)
        ]);

        yield MenuItem::section('Hobby');
        yield MenuItem::subMenu('Skills', 'fas fa-user-tie')->setSubItems([
            MenuItem::linkToCrud('Show HardSkills', 'fas fa-eye', HardSkills::class)->setAction(Crud::PAGE_INDEX),
            MenuItem::linkToCrud('Add HardSkills', 'fas fa-plus', HardSkills::class)->setAction(Crud::PAGE_NEW),
            MenuItem::linkToCrud('Show SoftSkills', 'fas fa-eye', SoftSkills::class)->setAction(Crud::PAGE_INDEX),
            MenuItem::linkToCrud('Add SoftSkills', 'fas fa-plus', SoftSkills::class)->setAction(Crud::PAGE_NEW),
        ]);
    }
}
