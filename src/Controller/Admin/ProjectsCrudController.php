<?php

	namespace App\Controller\Admin;

	use App\Entity\Projects;
	use Doctrine\ORM\EntityManagerInterface;
	use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
	use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
	use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
	use EasyCorp\Bundle\EasyAdminBundle\Context\AdminContext;
	use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
	use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
	use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
	use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
	use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
	use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
	use EasyCorp\Bundle\EasyAdminBundle\Field\IdField;
	use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
	use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
	use EasyCorp\Bundle\EasyAdminBundle\Field\LanguageField;
	use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
	use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
	use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
	use EasyCorp\Bundle\EasyAdminBundle\Field\UrlField;
	use EasyCorp\Bundle\EasyAdminBundle\Router\AdminUrlGenerator;
	use Symfony\Component\HttpFoundation\Response;

	class ProjectsCrudController extends AbstractCrudController
	{
		public const PROJECT_BASE_PATH  = 'upload/images/projects';
		public const PROJECT_UPLOAD_DIR = 'public/upload/images/projects';

		public static function getEntityFqcn(): string
		{
			return Projects::class;
		}

		public function configureFields(string $pageName): iterable
		{

			return [
				ImageField::new('image')
						  ->setBasePath(self::PROJECT_BASE_PATH)
						  ->setUploadDir(self::PROJECT_UPLOAD_DIR)
						  ->setSortable(false),

				TextField::new('name', 'Project name'),
				ArrayField::new('languages', 'Technologies'),
				BooleanField::new('team', 'Realized in team')->renderAsSwitch(false),
				TextareaField::new('description', 'Description of the project'),
				IntegerField::new('progress', 'Progression'),
				DateTimeField::new('createdAt', 'Creation date'),
				DateTimeField::new('updatedAt', 'Update date'),
			];
		}
	}
