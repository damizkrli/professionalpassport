<?php

namespace App\Controller\Admin;

use App\Entity\Study;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class StudyCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Study::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name', 'Name of the institution'),
            TextField::new('program', 'Program'),
            BooleanField::new('achieved', 'Diploma obtained')->renderAsSwitch(false),
            DateTimeField::new('date', 'End date')->renderAsNativeWidget(),
            TextEditorField::new('Description'),
        ];
    }
}
