<?php

namespace App\Controller\Admin;

use App\Entity\HardSkills;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\IntegerField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;

class HardSkillsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return HardSkills::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            IntegerField::new('progress')
        ];
    }
}
